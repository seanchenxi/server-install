#!/bin/bash
echo "Initializing servers..."

echo "Updating installed packages..."
apt-get update && apt-get upgrade -y

echo "Install usefull services..."
apt-get install -y logwatch=7.4.3+git20161207-2 fail2ban=0.9.6-2 backup-manager=0.7.12-4 postfix=3.1.6-0+deb9u1

apt-get install -y apache2 mariadb-server
apt-get install -y php7.0 libapache2-mod-php7.0 php7.0-mysql php7.0-curl php7.0-json php7.0-gd php7.0-mcrypt php7.0-intl php7.0-sqlite3 php7.0-gmp php7.0-mbstring php7.0-xml php7.0-zip
